[NAME]
runuser \- run a shell with substitute user and group IDs
[DESCRIPTION]
.\" Add any additional description here
[SEE ALSO]
.TP
More detailed Texinfo documentation could be found by command
.TP
\t\fBinfo coreutils \(aqsu invocation\(aq\fR\t
.TP
since the command \fBrunuser\fR is trimmed down version of command \fBsu\fR.
.br
